<?php

namespace App\Http\Controllers;
use App\Models\BukuModel;
use App\Models\MahasiswaModel;
use App\Models\TransaksiModel;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Type\Decimal;

class TabelTransaksiController extends Controller
{

    public function read()
    {
        $transaksi = DB::table('transaksi')
                    ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
                    ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id')
                    ->select('transaksi.*', 'mahasiswa.nama', 'buku.judul_buku', 'transaksi.status_pinjam', 'transaksi.tanggal_pinjam', 'transaksi.tanggal_kembali', 'transaksi.total_biaya')
                    ->get();
        $jumlah_transaksi = DB::table('transaksi')->count();
        return view('/tabeltransaksi', [
            'id_page' => 'transaksi',
            'transaksi' => $transaksi,
            'jml_transaksi' => $jumlah_transaksi
        ]);
    }

    public function addData()
    {
        $buku = DB::table('buku')->get();
        $mahasiswa = DB::table('mahasiswa')->get();
        return view('/tambahtransaksi', [
            'id_page' => 'transaksi',
            'mahasiswa' => $mahasiswa,
            'buku' => $buku
        ]);
    }

    public function create(Request $request) {
        $localzone = Carbon::now();
        $localtime = Carbon::parse($localzone)->format('Y-m-d H:i:s');

        DB::table('transaksi')->insert([
            'id_mahasiswa' => $request->mahasiswa,
            'id_buku' => $request->buku,
            'tanggal_pinjam' => $localtime,
            'status_pinjam' => 'dipinjam',
            'total_biaya' => 0
        ]);

        DB::table('buku')
        ->where('id', $request->buku)
        ->decrement('stok_buku');

        return redirect('/transaksi');
    }



    public function update($id) {
        $transaksi = DB::table('transaksi')
                    ->select('transaksi.id', 'transaksi.id_mahasiswa', 'transaksi.id_buku', 'mahasiswa.nama', 'buku.judul_buku', 'transaksi.status_pinjam', 'transaksi.tanggal_pinjam', 'transaksi.tanggal_kembali', 'transaksi.total_biaya')
                    ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
                    ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id')
                    ->where('transaksi.id', $id)
                    ->get();

        $buku = DB::table('buku')
                ->select('buku.id', 'buku.judul_buku')
                ->join('transaksi', 'buku.id', '!=', 'transaksi.id_buku')
                ->where('transaksi.id', $id)
                ->get();

        $mahasiswa = DB::table('mahasiswa')
                    ->select('mahasiswa.id', 'mahasiswa.nama')
                    ->join('transaksi', 'mahasiswa.id', '!=', 'transaksi.id_mahasiswa')
                    ->where('transaksi.id', $id)
                    ->get();

        return view('updatetransaksi', [
            'id_page' => 'transaksi',
            'transaksi' => $transaksi,
            'mahasiswa' => $mahasiswa,
            'buku' => $buku
        ]);
    }

    public function updating(Request $request) {
        DB::table('transaksi')
        ->where('id', $request->id_trans)
        ->update([
            'id_mahasiswa' => $request->mahasiswa,
            'id_buku' => $request->buku
        ]);
        return redirect('/transaksi');
    }

    public function updating_status(Request $request) {

        $to = Carbon::parse($request->tanggal_kembali);
        $from = Carbon::parse($request->tanggal_pinjam);
        $diffDays = $to->diffInDays($from);
        print_r($diffDays);

        $getPrice = DB::table('transaksi')
                ->select('buku.biaya_sewa_harian')
                ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
                ->where('transaksi.id', $request->id_trans)
                ->get();

        $price = intval($getPrice->first()->biaya_sewa_harian);

        $totalBiaya = $diffDays * $price;

        DB::table('transaksi')
        ->where('id', $request->id_trans)
        ->update([
            'status_pinjam' => $request->status,
            'tanggal_pinjam' => $request->tanggal_pinjam,
            'tanggal_kembali' => $request->tanggal_kembali,
            'total_biaya' => $totalBiaya
        ]);

        DB::table('buku')
        ->where('id', $request->id_buku)
        ->increment('stok_buku');

        return redirect('/transaksi');
    }

    public function delete($id) {
        DB::table('transaksi')->where('id', $id)->delete();
        return redirect('/transaksi');
    }


}
