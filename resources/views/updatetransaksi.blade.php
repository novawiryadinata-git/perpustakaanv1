@extends('layout2.template')
@section('title', 'Edit Data Transaksi')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header"><h3 class="text-center font-weight-light my-4">Edit Data Transaksi</h3></div>
                <div class="card-body">
                    @foreach ($transaksi as $trans)
                    <form action="/transaksi/updating" method="post">
                        {{ csrf_field() }}
                        <div><input type="hidden" name="id-trans" value="{{ $trans->id }}"><br /></div>

                        <div class="form-floating mb-3">
                            <select class="form-control" name="mahasiswa" id="mahasiswa" required>
                                <option value="">Pilih Mahasiswa</option>
                                @foreach ($mahasiswa as $mhs)
                                    <option value="{{$mhs->id}} "> {{$mhs->nama}} </option>
                                @endforeach
                            </select>
                            <label>Nama Mahasiswa</label>
                        </div>

                        <div class="form-floating mb-3">
                            <select  class="form-control" name="buku" id="buku" required>
                                <option value="">Pilih Buku</option>
                                @foreach ($buku as $bk)
                                    <option value="{{$bk->id}} "> {{$bk->judul_buku}} </option>
                                @endforeach
                            </select>
                            <label>Judul Buku</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Update Data</button>
                    </form>
                    @endforeach
                </div>
                <div class="card-body">
                    @foreach ($transaksi as $trans)
                    <form action="/transaksi/updating-status" method="post">
                        {{ csrf_field() }}
                        <div><input type="hidden" name="id_trans" value=" {{$trans->id}} "><br/></div>
                        <div><input type="hidden" name="id_buku" value=" {{$trans->id_buku}} "><br/></div>

                        <div class="form-floating mb-3">
                            <select class="form-control" name="status" id="status" required>
                                <option value="{{$trans->status_pinjam}}">{{ $trans->status_pinjam}}</option>
                                <option value="dikembalikan">dikembalikan</option>
                            </select>
                            <label>Status Pinjam</label>
                        </div>
                        <div class="form-floating mb-3 mb-md-0">
                            <select name="tanggal_pinjam"  id="tgl_pinjam" class="form-control" required>
                                <option value="{{$trans->tanggal_pinjam}}">{{ $trans->tanggal_pinjam}}</option>
                            </select>
                            <label>Tanggal Pinjam</label>
                        </div>
                        <br>

                        <div class="form-floating mb-3 mb-md-0 ">
                            <input id="tanggal_kembali" name="tanggal_kembali" class="form-control" type="date" placeholder="Tanggal Kembali" required autocomplete="tanggal_kembali" autofocus />
                            <label>Tanggal Kembali</label>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Update Status</button>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>





@endsection

{{--
@extends('layout2.template')
@section('title','Edit Data Buku')
@section('content')
    <div class="container-fluid">

        <div class="d-flex align-items-center justify-content-between mb-4">
            <h1 class="h4 mb-0">Update Data Transaksi</h1>
            <a href="{{ route('transaksi.index') }}" class="btn btn-danger btn-sm btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Back To List</span>
            </a>
        </div>
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">Update Data Transaksi</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    @foreach ($transaksi as $trans)
                        <form class="user" action="/transaksi/updating" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_trans" value="{{$trans->id}}">

                            <label for="mahasiswa" class="font-weight-bold">Nama Mahasiswa</label>
                            <div class="form-group">
                                <select id="mahasiswa" class="form-control" name="mahasiswa" required
                                    autofocus>
                                    <option value="{{$trans->id_mahasiswa}}">{{ $trans->nama}}</option>
                                    @foreach($mahasiswa as $mhs)
                                        <option value="{{ $mhs->id }}">{{ $mhs->nama}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label for="buku" class="font-weight-bold">Judul Buku</label>
                            <div class="form-group">
                                <select id="buku" class="form-control" name="buku" required
                                    autofocus>
                                    <option value="{{$trans->id_buku}}">{{ $trans->judul_buku}}</option>
                                    @foreach($buku as $bk)
                                        <option value="{{ $bk->id }}">{{ $bk->judul_buku}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <a href="/transaksi/updating"><button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Close</button></a>
                            <button type="submit" class="btn btn-primary">Update Data</button>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">Update Data Status Peminjaman</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    @foreach ($transaksi as $trans)
                        <form class="user" action="/transaksi/updating-status" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_trans" value="{{$trans->id}}">
                            <input type="hidden" name="id_buku" value="{{$trans->id_buku}}">

                            <label for="status" class="font-weight-bold">Status Peminjaman</label>
                            <div class="form-group">
                                <select id="status" class="form-control" name="status" required
                                    autofocus>
                                    <option value="{{$trans->status_pinjam}}">{{ $trans->status_pinjam}}</option>
                                    <option value="dikembalikan">dikembalikan</option>
                                </select>
                            </div>

                            <label for="tanggal_pinjam" class="font-weight-bold">Tanggal Peminjaman</label>
                            <div class="form-group">
                                <select id="tgl_pinjam" class="form-control" name="tanggal_pinjam" required
                                    autofocus>
                                    <option value="{{$trans->tanggal_pinjam}}">{{ $trans->tanggal_pinjam}}</option>
                                </select>
                            </div>
                            <label for="tanggal_kembali" class="font-weight-bold">Tanggal Pengembalian</label>
                            <div class="form-group">
                                <input id="tanggal_kembali" type="date" class="form-control"
                                    name="tanggal_kembali" required autocomplete="tanggal_kembali" autofocus>
                            </div>
                            <a href="/transaksi/updating-status"><button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Close</button></a>
                            <button type="submit" class="btn btn-primary">Update Status</button>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection --}}
