@extends('layout.template')
@section('title','Tabel Mahasiswa')
@section('main')

    <div class="container-fluid px-4">
        <h1 class="mt-4">Mahasiswa</h1>

        <div class="card mb-4">
            <div class="card-header d-flex align-items-center justify-content-between small">
                <div>
                    <i class="fas fa-table me-1"></i>
                    Data Mahasiswa
                </div>
                <div>
                    <a href="/home/create" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                </div>

            </div>

            <div class="card-body">

                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>E-Mail</th>
                            <th>No Telp</th>
                            <th>Prodi</th>
                            <th>Jurusan</th>
                            <th>Fakultas</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($mahasiswa as $mhs)
                            <tr>
                                <td> {{ $mhs->nama }} </td>
                                <td> {{ $mhs->nim }} </td>
                                <td> {{ $mhs->email }} </td>
                                <td> {{ $mhs->no_telp }} </td>
                                <td> {{ $mhs->prodi }} </td>
                                <td> {{ $mhs->jurusan }} </td>
                                <td> {{ $mhs->fakultas }} </td>
                                <td style="text-align: center;" class="card-header d-flex align-items-center justify-content-between small"  >
                                    <a href="/home/editmahasiswa/{{$mhs->id}}" class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <a href="/home/delete/{{$mhs->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection
