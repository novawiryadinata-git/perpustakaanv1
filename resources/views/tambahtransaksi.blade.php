@extends('layout2.template')
@section('title','Tambah Data Buku')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Tambah Transaksi</h3></div>
                    <div class="card-body">
                        <form action="/transaksi/simpan" method="POST">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <select name="mahasiswa" id="mahasiswa" required>
                                    <option value="">Pilih Mahasiswa</option>
                                    @foreach ($mahasiswa as $mhs)
                                        <option value="{{$mhs->id}} "> {{$mhs->nama}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-floating mb-3">
                                <select name="buku" id="buku" required>
                                    <option value="">Pilih Buku</option>
                                    @foreach ($buku as $buku )
                                        @if ($buku->stok_buku <= 0)
                                            <option disabled>{{$buku->stok_buku}}</option>
                                        @else
                                            <option value="{{$buku->id}} "> {{$buku->judul_buku}} </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-floating mb-3 mb-md-0">
                                    <input name="tanggal_dipinjam" class="form-control" type="date" placeholder="Tanggal Dipinjam" />
                                    <label>Tanggal Dipinjam</label>
                                </div>
                            </div> --}}
                            <div class="mt-4 mb-0">
                                <input class="d-grid btn btn-primary btn-block " href="" value="Tambah" type="submit">

                                {{-- <div class="d-grid"><a class="btn btn-primary btn-block" href="login.html">Tambah Data</a></div> --}}
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
