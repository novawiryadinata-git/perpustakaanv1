@extends('layout.template')
@section('title','Tabel Transaksi')
@section('main')

            <div class="container-fluid px-4">
                <h1 class="mt-4">Transaksi</h1>

                <div class="card mb-4">
                    <div class="card-header d-flex align-items-center justify-content-between small">
                        <div>
                            <i class="fas fa-table me-1"></i>
                            Data Transaksi
                        </div>
                        <div>
                            <a href="/transaksi/create" class="btn btn-primary "><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table id="datatablesSimple">
                            <thead>
                                <tr>
                                    <th>Nama Mahasiswa</th>
                                    <th>Judul Buku</th>
                                    <th>Status Pinjam</th>
                                    <th>Tanggal Pinjam</th>
                                    <th>Tanggal Kembali</th>
                                    <th>Total Biaya</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transaksi as $trsk)
                                <tr>
                                    <td> {{$trsk->nama}} </td>
                                    <td> {{$trsk->judul_buku}} </td>
                                    <td> {{$trsk->status_pinjam}} </td>
                                    <td> {{$trsk->tanggal_pinjam}} </td>
                                    <td> {{$trsk->tanggal_kembali == NULL ? '-' : $trsk->tanggal_kembali}} </td>
                                    <td>Rp {{$trsk->total_biaya}}</td>
                                    <td>
                                        <div class="d-flex justify-content-center">
                                            @if($trsk->status_pinjam == 'dipinjam')
                                                <a href="transaksi/update/{{$trsk->id}}" class="btn btn-warning btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            @else
                                                <a href="/transaksi/delete/{{$trsk->id}}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure delete this data?')">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            {{-- wadiadasd --}}
            <div class="modal fade" ></div>

@endsection
